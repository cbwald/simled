#include <stdio.h>
#include <unistd.h>
#include <errno.h>

#define US_CNT	1000000

char *ledName[] = {
		"RED",
		"BLUE",
		"GREEN"
};

#define NUM_LED	3

/****
 * Routine to pause execution.
 * Inputs: useconds_t - micro seconds to pause
 *
 * Returns: 0 - success
 *          fail code from usleep()
 */
int delay (useconds_t time)
{
	int rc = 0;
	rc = usleep (time);

	if (0 != rc) {
		perror ("usleep time failed");
	}
	return (rc);
}


int main (int argc, char **argv)
{
	int rc = 0;

	int ledIdx = 0; 	/* index for led[] */
	int ledCnt = 0;     /* counter */
	int offOnState = 0; /* state variable for on or off */

	/* Start, any error code exit program */
	while (0 == rc) {

		/* change state:
		 * if state is off then turn led off
		 * if state is on then choose next color and turn on
		 * then delay
		 */

		offOnState ^= 1;

		printf ("LED State : ");

		if (offOnState) {
			/* turn on - choose next LED */
			ledIdx = ledCnt++ % NUM_LED;
			printf ("ON %s\n", ledName[ledIdx]);
		}
		else {
			/* turn off */
			printf ("OFF\n");
		}

		rc = delay (US_CNT);
		if (0 != rc) {
			printf ("Delay call failed - quit");
		}
	}

	return (rc);
}
