# README #

LED Simulator demonstrating a forever loop that changes a tri-state LED on and off via 
a printf statement. There is a 1 second delay between state changes.


Step 1. Download 
git clone https://cbwald@bitbucket.org/cbwald/simled.git

Step 2. Build
cd simled/simLed/Debug
make

Step 3. Run
./simLed

Ctl-C  to stop.